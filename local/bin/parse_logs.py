#!/usr/bin/env python3

import os
import re
from sys import argv, stdin
from argparse import ArgumentParser, RawDescriptionHelpFormatter

parser = ArgumentParser(description="""
Parse log from shovill assembly pipeline and log from gam-n50.
Extract meaningfull values and join the together
in output.
Read shovill log from stdin
""",
formatter_class=RawDescriptionHelpFormatter)
parser.add_argument('gam_log', help='Log from gam-n50')

args = parser.parse_args()


removed_contigs = 0
read_stats = []
estimated_genome_size = ''
estimated_coverage = ''
subsampling_coverage = 'not request'
subsampling_factor = 'not request'
processed_reads = ''
new_coverage = ''
corrected_bases = ''
min_pairs_overlap = ''
combined_pairs = ''
uncombined_pairs = ''
frac_combined = ''
assembly_time = []
assembly_kmers_used = []
total_alignments = ''
removed_alignments = ''
repaired_contigs = ''
repaired_positions = ''
total_assembly_length = ''
frac_variation_on_estimated = ''
total_contigs = ''
min_contig_length = ''


for line in stdin:
	if(re.search(r'Read stats',line)):
		chunks = line.strip().replace('=','').split()
		read_stats.append('%s\t%s' %(chunks[3], chunks[4]))

	if(re.search(r'Using genome size',line)):
		chunks = line.strip().split()
		estimated_genome_size = chunks[4]

	if(re.search(r'Estimated sequencing depth',line)):
		chunks = line.strip().split()
		estimated_coverage = chunks[4]

	if(re.search(r'Subsampling reads by factor',line)):
		chunks = line.strip().split()
		subsampling_coverage = chunks[11].replace('x','')
		subsampling_factor = chunks[5]

	if(re.search(r'\[lighter\] Processed\s\d+\sreads',line)):
		chunks = line.strip().split()
		processed_reads = chunks[2]

	if(re.search(r'Average coverage is',line)):
		chunks = line.strip().split()
		new_coverage = chunks[6]

	if(re.search(r'Corrected\s\d+\sbases',line)):
		chunks = line.strip().split()
		corrected_bases = chunks[2]

	if(re.search(r'Min overlap',line)):
		chunks = line.strip().split()
		min_pairs_overlap = chunks[3]

	if(re.search(r'Combined pairs',line)):
		chunks = line.strip().split()
		combined_pairs = chunks[3]

	if(re.search(r'Uncombined pairs',line)):
		chunks = line.strip().split()
		uncombined_pairs = chunks[3]

	if(re.search(r'Percent combined',line)):
		chunks = line.strip().split()
		frac_combined = chunks[3].replace('%','')

	if(re.search(r'Assembling time',line)):
		chunks = line.strip().split()
		assembly_time.append(chunks[1])

	if(re.search(r'Assembling finished. Used k-mer sizes',line)):
		chunks = line.strip().replace(',','').split()
		assembly_kmers_used = chunks[7:]

	if(re.search(r'Total SAM records',line)):
		chunks = line.strip().replace(',','').split()
		total_alignments = chunks[5]
		removed_alignments = chunks[7]

	if(re.search(r'Repaired\s\d+\scontigs',line)):
		chunks = line.strip().split()
		repaired_contigs = chunks[2]
		repaired_positions = chunks[7]

	if(re.search(r'Removing short contig',line)):
		chunks = line.strip().split()
		removed_contigs+=1

	if(re.search(r'Assembly is\s\d+',line)):
		chunks = line.strip().replace(',','').split()
		total_assembly_length = chunks[3]
		frac_variation_on_estimated = chunks[-1].replace('(','').replace(')','').replace('%','')

	if(re.search(r'It contains\s\d+',line)):
		chunks = line.strip().split()
		total_contigs = chunks[3]
		min_contig_length = chunks[4].split('=')[1].replace(')','')


# print after parsing because sometime
# results appears in different order

## sample name in second column
print('', os.path.basename(os.getcwd()), sep='\t')

print('#reads stats:\t')
print('\n'.join(sorted(read_stats))) # need to sort because sometime results appears in different order

print('#coverage normalization:\t')
print('estimated_genome_size', estimated_genome_size, sep='\t')
print('estimated_coverage', estimated_coverage, sep='\t')
print('subsampling_coverage', subsampling_coverage, sep='\t')
print('subsampling_factor', subsampling_factor, sep='\t')
print('new_coverage', new_coverage, sep='\t')
print('subsampled_reads', processed_reads, sep='\t')

print('#reads error correction:\t')
print('corrected_bases', corrected_bases, sep='\t')

print('#reads joining:\t')
print('min_pairs_overlap', min_pairs_overlap, sep='\t')
print('combined_pairs', combined_pairs, sep='\t')
print('uncombined_pairs', uncombined_pairs, sep='\t')
print('%_combined', frac_combined, sep='\t')

print('#per kmer assembly time:\t')
[ print(k,t, sep='\t') for k, t in zip(assembly_kmers_used, assembly_time) ]

print('#contigs error correction:\t')
print('total_alignments', total_alignments, sep='\t')
print('removed_alignments', removed_alignments, sep='\t')
print('repaired_contigs', repaired_contigs, sep='\t')
print('repaired_positions', repaired_positions, sep='\t')
print('removed too_short', removed_contigs, sep='\t')

print('#assembly stats:\t')
print('total_assembly_length', total_assembly_length, sep='\t')
print('%_variation_on_estimated', frac_variation_on_estimated, sep='\t')

# rest of information by parsing 1bp.contigs.gam
with open(args.gam_log, 'r') as f:
	for i, line in enumerate(f):
		if i > 1:
			print('\t'.join(line.strip().replace(' ','_').split('_=_')))