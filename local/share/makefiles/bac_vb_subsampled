# Copyright 2019 Michele Vidotto <michele.vidotto@gmail.com>

# conda environment
CONDA_ENV := /igatech/rd/dev/miniconda3

# load conda environment
define load_env
	module purge; \
	__conda_setup="$$(CONDA_REPORT_ERRORS=false '$(CONDA_ENV)/bin/conda' shell.bash hook 2> /dev/null)"; \
	if [ $$? -eq 0 ]; then \
		\eval "$$__conda_setup"; \
	else \
		if [ -f "$(CONDA_ENV)/etc/profile.d/conda.sh" ]; then \
			. "$(CONDA_ENV)/etc/profile.d/conda.sh"; \
			CONDA_CHANGEPS1=false conda activate base; \
		else \
			\export PATH="$(CONDA_ENV)/bin:$$PATH"; \
		fi; \
	fi; \
	conda activate "$(CONDA_ENV)/envs/assembly_denovo_tmp"
endef

# sequencing length for filtering contigs and scaffolds
FILTER_LENGTH := 1

SUBSAMPLE_READS := 100000

# must be initialize as empty
SAMPLES := 


SAMPLE := BACVB1
NULL := $(call set,$(SAMPLE),1,/igatech/workspace/pipeline_results/non-human_wgs/2021/ID2104_Paonessa_vectors/trim_align-erne_filter_bbduk_multiqc/trimmed_bac_vb/68227_ID2104_1-BACVB1_S1_L001_clean_1.fastq.gz)
NULL := $(call set,$(SAMPLE),2,/igatech/workspace/pipeline_results/non-human_wgs/2021/ID2104_Paonessa_vectors/trim_align-erne_filter_bbduk_multiqc/trimmed_bac_vb/68227_ID2104_1-BACVB1_S1_L001_clean_2.fastq.gz)
SAMPLES += $(SAMPLE)

SAMPLE := BACVB2
NULL := $(call set,$(SAMPLE),1,/igatech/workspace/pipeline_results/non-human_wgs/2021/ID2104_Paonessa_vectors/trim_align-erne_filter_bbduk_multiqc/trimmed_bac_vb/68228_ID2104_2-BACVB2_S1_L001_clean_1.fastq.gz)
NULL := $(call set,$(SAMPLE),2,/igatech/workspace/pipeline_results/non-human_wgs/2021/ID2104_Paonessa_vectors/trim_align-erne_filter_bbduk_multiqc/trimmed_bac_vb/68228_ID2104_2-BACVB2_S1_L001_clean_2.fastq.gz)
SAMPLES += $(SAMPLE)

SAMPLE := BACVB3
NULL := $(call set,$(SAMPLE),1,/igatech/workspace/pipeline_results/non-human_wgs/2021/ID2104_Paonessa_vectors/trim_align-erne_filter_bbduk_multiqc/trimmed_bac_vb/68229_ID2104_3-BACVB3_S1_L001_clean_1.fastq.gz)
NULL := $(call set,$(SAMPLE),2,/igatech/workspace/pipeline_results/non-human_wgs/2021/ID2104_Paonessa_vectors/trim_align-erne_filter_bbduk_multiqc/trimmed_bac_vb/68229_ID2104_3-BACVB3_S1_L001_clean_2.fastq.gz)
SAMPLES += $(SAMPLE)

SAMPLE := BACVB4
NULL := $(call set,$(SAMPLE),1,/igatech/workspace/pipeline_results/non-human_wgs/2021/ID2104_Paonessa_vectors/trim_align-erne_filter_bbduk_multiqc/trimmed_bac_vb/68233_ID2104_7-BACVB4_S1_L001_clean_1.fastq.gz)
NULL := $(call set,$(SAMPLE),2,/igatech/workspace/pipeline_results/non-human_wgs/2021/ID2104_Paonessa_vectors/trim_align-erne_filter_bbduk_multiqc/trimmed_bac_vb/68233_ID2104_7-BACVB4_S1_L001_clean_2.fastq.gz)
SAMPLES += $(SAMPLE)