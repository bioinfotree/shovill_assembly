# Copyright 2019 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

SAMPLES ?=

# recall bmake
MAKE := bmake

1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
# this function install all the links at once
%.fastq.gz:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(foreach KEY,$(call keys,$(SAMPLE)), \
			$(shell ln -sf $(call get,$(SAMPLE),$(KEY)) $(SAMPLE).$(KEY).fastq.gz) \
		) \
	) \
	&& sleep 3

# Prepare directories and generate directories list
# setting up the variable DIR_LST
dir_lst.mk: makefile $(1_FASTQ_GZ) $(2_FASTQ_GZ)
	RULES_PATH="$${PRJ_ROOT}/local/share/rules/assembly.mk"; \
	if [ -s $${RULES_PATH} ] && [ -s $< ]; then \
	  printf "DIR_LST :=" >$@; \
	  for SAMPLE in $(SAMPLES); do \
		mkdir -p $${SAMPLE}; \   * create dir for given sample *
		cd $${SAMPLE}; \
		ln -sf ../$<; \   * link makefile *
		ln -sf $${RULES_PATH} rules.mk; \   * link rules *
		ln -sf ../"$${SAMPLE}.1.fastq.gz" 1.fastq.gz; \   * link read 1 *
		ln -sf ../"$${SAMPLE}.2.fastq.gz" 2.fastq.gz; \   * link read 2 *
		cd ..; \
		printf " $${SAMPLE}" >>$@; \   * add dir name to list *
	  done; \
	fi

include dir_lst.mk

# execute the assembly on each subdirectory
subsystem.flag: dir_lst.mk
	@echo Looking into subdirs: $(MAKE)
	for D in $(DIR_LST); do \
	  if [ -d $${D} ]; then \
	    cd $${D}; \
	    $(MAKE) && \
	    cd ..; \
	  fi; \
	done; \
	touch $@

# regenerate all assembly statistics
# ("shovill.tsv") if needed
subsystem.flag2: dir_lst.mk
	@echo Looking into subdirs: $(MAKE)
	for D in $(DIR_LST); do \
	  if [ -d $${D} ]; then \
	    cd $${D}; \
	    $(MAKE) shovill.tsv && \
	    cd ..; \
	  fi; \
	done; \

# merge together all
# assemblies statistics
assemblies.stats: dir_lst.mk subsystem.flag
	>$@; \
	i=0; \
	for F in $$(find $(DIR_LST) -type f -name "shovill.tsv"); \
	do \
		if [ $$i -eq 0 ]; then \
			cat $$F >tmp; \   * take all columns from the first file *
		else \
			cut -f2 <$$F \   * select only second column from other files *
			| paste tmp - >$@; \
			cp $@ tmp; \
		fi; \
		let i+=1; \
	done; \
	rm tmp


.PHONY: test
test:
	@echo


ALL +=  $(1_FASTQ_GZ) \
        $(2_FASTQ_GZ) \
		subsystem.flag \
		assemblies.stats

CLEAN +=

# add dipendence to clean targhet
clean: clean_dir

# recall bmake clean into each subdirectory
.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) clean; \
	    cd ..; \
	  fi; \
	done
