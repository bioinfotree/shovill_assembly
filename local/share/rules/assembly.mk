# Copyright 2019 Michele Vidotto <michele.vidotto@gmail.com>

# need for filter_contigs
context prj/lpneumophila/spades_assembly

tmp:
	mkdir -p $@

# assembly with shovill pipeline
contigs.fa: tmp 1.fastq.gz 2.fastq.gz
	!threads
	$(call load_env); \
	shovill \
	--R1 $^2 \
	--R2 $^3 \
	--outdir "$${PWD}" \
	--tmpdir "$${PWD}/$<" \
	--force \
	--cpus "$${THREADNUM}"
#	--keepfiles \

contigs.gfa shovill.corrections spades.fasta shovill.log: contigs.fa
	touch $@

# filter for scaffolds or contigs length
$(FILTER_LENGTH)bp.%.fasta.gz: %.fa
	module purge; \
	filter_contigs --fasta $< --length $(FILTER_LENGTH) \
	| gzip -c9 >$@

# get statistics
$(FILTER_LENGTH)bp.%.gam: $(FILTER_LENGTH)bp.%.fasta.gz
	module purge; \
	module load sw/assemblers/gam-ngs/default; \
	gam-n50 <(zcat $<) \
	| tr \\t \\n >$@

# extract information into unique log file
shovill.tsv: shovill.log 1bp.contigs.gam
	$(call load_env); \
	parse_logs.py $^2 <$< >$@

ALL += 1.fastq.gz \
       2.fastq.gz \
       1bp.contigs.fasta.gz \
       1bp.contigs.gam \
	   contigs.gfa \
       shovill.corrections \
       shovill.log \
	   shovill.tsv \
       spades.fasta

INTERMEDIATE += contigs.fa

CLEAN += tmp